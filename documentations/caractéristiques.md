
### Caractéristiques obligatoires

L'application permetterait de "traquer ses macronutriments (1)". L'idée derrière est de pouvoir avoir un suivi calorique journalier , ainsi que les détails des macronutriments en fonction des aliments consommés.

L'utilisateur de l'application pourrait : 
- créer un profil et le modifier selon ses besoins.
	- référencer diverses informations générales qui l'identifieraient aux yeux des autres utilisateurs : Nom, prénom, pseudonyme, date de naissance, sexe.
	- référencer diverses informations sur sa santé générale : activité(s) sportive(s), niveau d'activité (sédentaire, faiblement actif, actif, très actif), poids actuel, taille.
	- loader une photo de profil.
	- changer ses informations en tout temps (mot de passe, pseudonyme, e-mail, date de naissance, sexe, prénom, nom).
	- consulter son profil ainsi que son "mur" d'activités. Le mur d'activités contient les repas loggés. 
	- chercher le profil d'un utilisateur selon son pseudo.
	- ajouter/supprimer un utilisateur en tant qu'ami.
	- rendre son profil privé, visible uniquement par les amis, public.

- créer un "repas" et ainsi logger quotidiennement l'heure du repas, le nombre de calories ingérées ainsi que ses macronutriments.
	- rechercher un aliment dans la base de données.
	- ajouter un aliment dans la base de données.
		- un aliment est composé de : 
			- Lipides (Acides gras saturés, acides gras polyinsaturés, acides gras monoinsaturés, acides gras trans)
			- Cholestérol
			- Sodium
			- Potassium
			- Glucides
			- Fibres alimentaires
			- Sucres
			- Protéines
			- Vitamine A, B, C, ...
			- Calcium
			- Fer	
	- modifier/supprimer un aliment qu'il a rentré dans la base de données.
	- renseigner l'heure du repas.
	- modifier un repas

- renseigner sa consommation d'eau
	- choisir la taille du récipient
	- ajouter/supprimer une portion d'eau bue

- Consulter les estimations faites par l'application concernant ses besoins alimentaires journaliers. Il aurait ainsi accès à :
	- son total calorique journalier recommandé
	- la quantité de macronutriments recommandés
	- Un graphique représentant les calories et macronutriments consommés selon les recommandations médicales. Le graphique est mis à jour à chaque entrée d'un nouvel aliment.
	- Un graphique représentant l'évolution de ses apports journaliers, hebdomadaire, mensuel, annuel.
	- Un graphique représentant l'évolution de son poids, hebdomadaire, mensuel, annuel.


### Caractéristiques facultatives

- Définir un objectif.
	- choisir entre les options : prise de masse, perte de poids, maintient du poids.
	- changer l'option quand l'utilisateur le souhaite. 
	- accéder aux recommandations médicales générées selon l'option choisie.
		- nombre de calories
		- quantité de macronutriments
- Créer/Supprimer/Rejoindre/Consulter un événement.
	- un événement sportif est composé de diverses informations : nom, date de début, date de fin, description, éventuel tarif, nombre de participants min/max, nombre de participants actuel, catégorie de sport (général, kanoe-kayak, tennis, etc...).
	- modifier un événement si l'utilisateur en est le créateur.
	- accéder à la liste des événements que l'utilisateur a rejoint.
- Poster/Modifier/Supprimer/Consulter une activité personnelle.
	- Une activité personnelle peut être de forme diverse : texte, image, mix des deux, ...
	- Liker/Commenter l'activité d'un utilisateur.
- Accéder à sa messagerie.
	- Consulter les messages envoyés/reçus.
	- Pouvoir sélectionner un utilisateur et lui envoyer un message.
	- Pouvoir supprimer un message de sa boite de réception.
- Accéder à l'onglet recettes.
	- Pouvoir consulter/Créer/Modifier/Supprimer une recettes
	- Pouvoir mettre une recette en favoris. 
	- Pouvoir noter une recette de 1 à 5
	- Pouvoir trier les recettes selon différents critères (notes, calories, glucides, lipides, ...).
- Accéder à la version mobile.

#### Notes

(1) Parmi les macronutriments, appelés aussi macros, on entend les sources majeures d’énergie à savoir les glucides, les lipides et les protéines. Ensemble, ils forment la base même de tous les processus métaboliques et sont par conséquent vitaux. L’apport de ces trois nutriments se fait par la nutrition.