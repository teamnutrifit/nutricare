Cette page référence toutes les fonctionnalités que peut réaliser l'application. Elles sont classées entre "Obligatoire" et "Facultative".
Les fonctionnalités obligatoire seront présentes dans l'application, tandis que les fonctionnalités facultatives seront implémentées si le temps nous le permet. 

| Idée| Obligatoire | Facultative |
| ---- |:------------:| :------------:|
| Création/Modification/Suppression d'un profil  | X |   |
| Création/Modification/Suppression d'un repas | X |   |
| Rechercher un aliment | X |   |
| Ajouter/Modifier/Supprimer un aliment | X |   |
| Enregistrer/Supprimer un repas type | X |   |
| Renseigner sa consommation d'eau | X |   |
| Consulter les recommandations et progressions | X |   |
| Ajouter/Supprimer un utilisateur en ami | X |   |
| Définir un but |   | X |
| Organiser/Modifier/Supprimer un événement |   | X |
| Rejoindre un événement |   | X |
| Poster/Modifier/Supprimer une activité personnelle (texte, image, ...) |  | X |
| Liker/commenter l'activité d'un utilisateur |   | X |
| Messagerie |   | X |
| Onglet recettes |   | X |
| Ajouter/Modifier/Supprimer une recette |   | X |
| Mettre une recette en favoris |   | X |
| Classement de recettes |   | X |
| Voter pour une recette |   | X |
| Version mobile |   | X |
