# NutriCare

## Projet : Application de gestion d'apports nutritifs journaliers

### But

Le but de ce projet est de répondre aux besoins d'une personne souhaitant des outils qui l'aideraient à suivre sa nutrition.
L'application permettrait à la personne de référencer ses données pour avoir un suivi et se situer par rapport aux autres utilisateurs ainsi qu'aux recommandations médicales en vigueur.

## Mock-ups
Nous avons utilisé Figma pour réaliser les mock-ups de notre site, voici le lien de ce dernier : [mock-up de notre site](https://www.figma.com/file/nl9YIHRiCCWf34wBaB1cUe/Website?node-id=0%3A1)

Pour plus de précision, vous pouvez consulter le [wiki](https://gitlab.com/teamnutrifit/nutricare/-/wikis/home) !

## Landing page
Afin de vous donner l'eau à la bouche, nous avons crée une page de présentation de notre projet. Pour l'admirer, veuillez suivre [ce lien](https://teamnutrifit.gitlab.io/nutricare/).

## Repositories
Pour une meilleure organisation du projet, nous l'avons scinder en deux catégories : 

### Front-end
Pour voir la page GitLab, [cliquer ici](https://teamnutrifit.gitlab.io/nutricare-frontend).

https://gitlab.com/teamnutrifit/nutricare-frontend



### Back-end
https://gitlab.com/teamnutrifit/nutricare-backend